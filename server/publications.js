Meteor.publish('plantProblems', function(){
  return PlantProblem.find();
});

Meteor.publish('pests', function(){
  return PlantProblem.find({'type': 'Pest'});
});

Meteor.publish('diseases', function(){
  return PlantProblem.find({'type': 'Disease'});
});

Meteor.publish('singlePlantProblem', function(id) {
  check(id, String);
  return PlantProblem.find(id);
});

Meteor.publish('plantProblemBrowse', function(){
	// check(skipCount,Number);
	// console.log(skipCount);
	return PlantProblem.find({});
});

Meteor.publish('images', function(){ 
	return Images.find(); 
});
