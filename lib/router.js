Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  notFoundTemplate: 'notFound',
  
});

Router.route('/', {
  name: 'home',
});


Router.route('/pests', function () {
  this.render('SaraiPestsLib');
});

Router.route('/pests/:_id', {
  name: 'PestProfile',
  waitOn: function() {
    return [
      Meteor.subscribe('singlePlantProblem', this.params._id),
    ];
  },
  data: function() { return PlantProblem.findOne(this.params._id); }
});

Router.route('/search', function () {
  this.render('PlantProblemSearchForm');
});

Router.route('/diseases', function () {
  this.render('SaraiDiseasesLib');
});

Router.route('/diseases/:_id', {
  name: 'DiseaseProfile',
  waitOn: function() {
    return [
      Meteor.subscribe('singlePlantProblem', this.params._id),
    ];
  },
  data: function() { return PlantProblem.findOne(this.params._id); }
});

var requireLogin = function() {
  if (! Meteor.user()) {
    if (Meteor.loggingIn()) {
      this.render(this.loadingTemplate);
    } else {
      this.render('accessDenied');
    }
  } else {
    this.next();
  }
}
