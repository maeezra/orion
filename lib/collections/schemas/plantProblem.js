PlantProblem.attachSchema(new SimpleSchema({
  name: {
    type: String,
    optional: false,
    label: 'Name'
  },
  type: {
    type: String,
    optional: false,
    label: 'Type',
    allowedValues: ['Pest', 'Disease'],
  },
  eng_name: {
    type: String,
    optional: true,
    label: 'Common Names'
  },  
  fil_name: {
    type: String,
    optional: true,
    label: 'Filipino Names'
  },
  sci_name: {
    type: String,
    optional: true,
    label: 'Scientific Name'
  },
  order: {
    type: String,
    optional: true,
    label: 'Order'
  },
  classification: {
    type: String,
    optional: true,
    label: 'Classification'
  },
  stage_threatening: {
    type: String,
    optional: true,
    label: 'Threatening Stage'
  },
  plant_affected: {
    type: String,
    optional: false,
    label: 'Plant Affected'
  },
  description: {
    type: String,
    optional: true,
    label: 'Description',    
    autoform: {
      type: 'textarea',
    }
  },
  symptoms: {
    type: String,
    optional: true,
    label: 'Symptoms',
    autoform: {
      type: 'textarea',
    }
  },
  effect: {
    type: String,
    optional: true,
    label: 'Effect',
    autoform: {
      type: 'textarea',
    }
  },
  treatment: {
    type: String,
    optional: true,
    label: 'Treatment',
    autoform: {
      type: 'textarea',
    }
  },
  stage_plant_affected: {
    type: String,
    optional: true,
    label: 'Stage Plant Affected'
  },
  part_destroyed: {
    type: String,
    optional: true,
    label: 'Part Destroyed'
  },
  fil_stage_plant_affected: {
    type: String,
    optional: true,
    label: 'Fil Stage Plant Affected'
  },
  fil_effect: {
    type: String,
    optional: true,
    label: 'Fil Effect'
  },
  fil_part_destroyed: {
    type: String,
    optional: true,
    label: 'Fil Part Destroyed'
  },
  fil_stage_threatening: {
    type: String,
    optional: true,
    label: 'Fil Stage Threatening'
  },
  fil_symptoms: {
    type: String,
    optional: true,
    label: 'Fil Symptoms',
    autoform: {
      type: 'textarea',
    }
  },
  fil_description: {
    type: String,
    optional: true,
    label: 'Fil Description',
    autoform: {
      type: 'textarea',
    }
  },
  fil_plant_affected: {
    type: String,
    optional: true,
    label: 'Fil Plant Affected'
  },
  fil_classification: {
    type: String,
    optional: true,
    label: 'Fil Classification'
  },
  fil_treatment: {
    type: String,
    optional: true,
    label: 'Fil Treatment',
    autoform: {
      type: 'textarea',
    }
  },
  image: {
    type: String,
    optional: false,
    label: 'Image URL'
  }

}));