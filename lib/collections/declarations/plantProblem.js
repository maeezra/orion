PlantProblem = new orion.collection('plantProblem', {
  singularName: 'plantProblem', 
  pluralName: 'plantProblems', 
  link: {
    title: 'Plant Problems' 
  },
  tabular: {
    columns: [
      { 
        data: "name", 
        title: "Name" 
      },{ 
        data: "type", 
        title: "Type" 
      },{ 
        data: "eng_name", 
        title: "English Name" 
      },{ 
        data: "fil_name", 
        title: "Filipino Name" 
      },{ 
        data: "sci_name", 
        title: "Scientific Name" 
      }
    ]
  }
});


