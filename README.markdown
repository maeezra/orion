## Pests and Diseases Library ##

### System Overview ###
The Pests and Diseases Library is a web application which allows collecting information about a pest or disease of known crops in the Philippines. 

The application is part of Project SARAI which aims to provide information and immediate management control actions of a pest infestation.
	




### Specifications ###

The Pests and Diseases Library operates on any web browser. It requires installed Meteor.js on the operating system and connection to the Internet to be able to run the application. It is recommended to run the application on Linux. 

To install Meteor, install the latest official Meteor release from your terminal:


curl https://install.meteor.com/ | sh

To run it locally, open a terminal:


cd project


meteor

To import the database:


meteor mongo


Open another terminal then run,


mongorestore --port=3001 --collection plantProblem --db meteor /dump/meteor/plantProblem.bson


Open your web browser and go to http://localhost:3000 to see the app running.




### User Level Access ###

Everyone can use application, but only registered users can add, modify, and delete records.