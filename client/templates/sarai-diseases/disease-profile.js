import { Images } from '../../../imports/collections.js';

Template.DiseaseProfile.helpers({
	disease: function(){
		return PlantProblem.find(this.params._id);
	},
});



Template.DiseaseProfile.events({
	'click #update-info': function(event,template){
		const recordId = Router.current().params._id;

		Router.go('/admin/plantProblem/'+recordId);
	},

	'change .myFileInput': function(event, template) {
      FS.Utility.eachFile(event, function(file) {
        Images.insert(file, function (err, fileObj) {
          if (err){
             // handle error
          } else {
             // handle success depending what you need to do
            var userId = Meteor.userId();
            var imagesURL = {
              "profile.image": "/cfs/files/images/" + fileObj._id
            };

            const imageUrl = "/cfs/files/images/" + fileObj._id;
            const recordId = Router.current().params._id;

            Meteor.users.update(userId, {$set: imagesURL},function(error,success){
            	$('#loading-ind').show();
            	setTimeout(function(){

	               	PlantProblem.update(
						{_id : recordId},
						{
							$set:{
								image : imageUrl
							}
						},
						function(success){
						},
						{
							upsert:false
						}
					);
    				sAlert.success('You have successfully uploaded the image!', {position: 'bottom-right', timeout: '3500',});
    				$('#loading-ind').hide();
	         	}, 2000);
				

            });


          }
        });
     });
   },
});

