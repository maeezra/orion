Template.SaraiDiseasesLib.onCreated(function() {
	Meteor.subscribe('diseases');
	//Meteor.subscribe('cms');
});

Template.SaraiDiseasesLib.helpers({
	displayDiseases: function(){
		//const displayPest = PlantProblem.find({'type': 'Pest'}).fetch()
		//console.log(displayPests);
		return PlantProblem.find({'type': 'Disease'});
		
	}
})

Template.IndividualDisease.helpers({
	imageName: function(str){
		return str.replace(/\s/g, '');
	}
})
