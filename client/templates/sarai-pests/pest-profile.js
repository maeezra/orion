import { Images } from '../../../imports/collections.js';



Template.PestProfile.helpers({
	pest: function(){
		return PlantProblem.find(this.params._id);
	},

	/*equals: function(v1, v2) {
		return (v1 === v2);
	},
	searchPath: function(){
		return "home";
	},
	imageName: function(str){
		return str.replace(/\s/g, "");
	}*/
});

Template.PestProfile.events({
	'click #update-info': function(event,template){
		const recordId = Router.current().params._id;

		Router.go('/admin/plantProblem/'+recordId);
	},

	'change .myFileInput': function(event, template) {
      FS.Utility.eachFile(event, function(file) {
        Images.insert(file, function (err, fileObj) {
          if (err){
             // handle error
          } else {
             // handle success depending what you need to do
            var userId = Meteor.userId();
            var imagesURL = {
              "profile.image": "/cfs/files/images/" + fileObj._id
            };

            const imageUrl = "/cfs/files/images/" + fileObj._id;
            const recordId = Router.current().params._id;


            Meteor.users.update(userId, {$set: imagesURL},function(error,success){
            	$('#loading-ind').show();
            	setTimeout(function(){
	               	PlantProblem.update(
						{_id : recordId},
						{
							$set:{
								image : imageUrl
							}
						},
						function(success){
							Router.go('/pests/'+recordId);
						},
						{
							upsert:false
						}
					);
					sAlert.success('You have successfully uploaded the image!', {position: 'bottom-right', timeout: '3500',});
	         		$('#loading-ind').hide();
	         	}, 2000);
				


            });


          }
        });
     });
   },
});