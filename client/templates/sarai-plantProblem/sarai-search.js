Session.setDefault("skip",0);
globalFunc();
function globalFunc(){
	var_limit=12;
}

Template.PlantProblemSearchForm.onCreated(function() {
	Meteor.subscribe('plantProblemBrowse');
});

Template.PlantProblemSearchForm.events({
	'submit': function(e,t){
		console.log('submit');
		e.preventDefault();
		
		Session.set('d_cnameFilter', $('#cname').val());
		Session.set('d_fnameFilter', $('#fname').val());
		Session.set('snameFilter', $('#sname').val());
		Session.set('tstageFilter', $('#tstage').val());
		Session.set('d_paffectedFilter', $('#paffected').val());
		Session.set('rpestFilter', $('#rpest').val());
		Session.set('typeFilter', $('input[name=optradio]:checked').val());
		console.log(Session.get('typeFilter'));
	},


	'click .prev':function(e,t){
		console.log('prev clicked');
		e.preventDefault();
		if(Session.get('skip')>=var_limit){
			Session.set('skip', Session.get('skip')-var_limit);
			Session.set('page_number',Session.get('page_number')-1);	
		}
		
	},

	'click .nex':function(e,t){
		e.preventDefault();

		Session.set('skip', Session.get('skip')+var_limit);
		Session.set('page_number',Session.get('page_number')+1);	

	}
});

Template.PlantProblemSearchForm.rendered = function() {
    // Initialise tags input
    console.log('render')

    $('#cname').tagsinput({
    	tagClass:  'label label-primary'    		
    	
    });
    $("#fname").tagsinput({
    	tagClass:  'label label-primary'
    });

    $("#paffected").tagsinput({
    	tagClass:  'label label-primary'
    });
    $("#rpest").tagsinput({
    	tagClass:  'label label-primary'
    });

    $("#sname").tagsinput({
    	tagClass:  'label label-primary'
    });
    $("#tstage").tagsinput({
    	tagClass:  'label label-primary'
    });
};


Template.PlantProblemSearchForm.helpers({
	d_results: function(){
		var selector = {};
		d_cnameFilter=Session.get('d_cnameFilter')
		d_fnameFilter= Session.get('d_fnameFilter')
		d_paffectedFilter= Session.get('d_paffectedFilter')
		rpestFilter= Session.get('rpestFilter')
		snameFilter= Session.get('snameFilter')
		tstageFilter= Session.get('tstageFilter')
		typeFilter=Session.get('typeFilter')

		if (d_cnameFilter !== undefined && d_cnameFilter!=="") {
			d_cnameFilter=d_cnameFilter.split(',');
			d_cnameFilter = d_cnameFilter.map(function(e) {return new RegExp(e, "i")});
			selector.name={$in:d_cnameFilter};
		}

		if (d_fnameFilter !== undefined && d_fnameFilter!=="") {
			d_fnameFilter=d_fnameFilter.split(',');
			d_fnameFilter = d_fnameFilter.map(function(e) {return new RegExp(e, "i")});
			
			selector.fil_name={$in:d_fnameFilter};
		}
		if (d_paffectedFilter !== undefined && d_paffectedFilter!=="") {
			d_paffectedFilter=d_paffectedFilter.split(',');
			d_paffectedFilter = d_paffectedFilter.map(function(e) {return new RegExp(e, "i")});
			
			selector.plant_affected={$in:d_paffectedFilter};
		}
		if (rpestFilter !== undefined && rpestFilter!=="") {
			rpestFilter=rpestFilter.split(',');
			rpestFilter = rpestFilter.map(function(e) {return new RegExp(e, "i")});
			// selector.name.$in=rdiseaseFilter;
		}
		if (snameFilter !== undefined && snameFilter!=="") {
			snameFilter=snameFilter.split(',');
			snameFilter = snameFilter.map(function(e) {return new RegExp(e, "i")});
			selector.sci_name={$in:snameFilter};
		}
		if (tstageFilter !== undefined && tstageFilter!=="") {
			tstageFilter=tstageFilter.split(',');
			tstageFilter = tstageFilter.map(function(e) {return new RegExp(e, "i")});
			selector.stage_threatening={$in:tstageFilter};

		}

		//selector.type='Disease';
		if (typeFilter !== undefined && typeFilter!=="") {
			selector.type=typeFilter;
		}
		if(Session.get('skip')==0){
			Session.set('page_number', 1);	
		}
		data=PlantProblem.find( selector);
		result=PlantProblem.find( selector,{sort: {name: 1},limit:var_limit,skip:Session.get('skip')});
		Session.set('results_count', data.count());
		Session.set('current_page_count', result.count());
		return result;

	},

	result_count: function(){
		if(Session.get('current_page_count')<var_limit){
			no=Session.get('current_page_count')+ (Session.get('page_number')-1)*var_limit;
		}else{
			no=Session.get('current_page_count')*Session.get('page_number');
		}
		return "Displaying "+no+ " of "+Session.get('results_count');
	},
	set_next_active: function(){
		if(Session.get('current_page_count')<var_limit){
			no=Session.get('current_page_count')+ (Session.get('page_number')-1)*var_limit;
		}else{
			no=Session.get('current_page_count')*Session.get('page_number');
		}

		if(no==Session.get('results_count')){
			return 'disabled';
		}
	},
	set_prev_active: function(){
		

		if(Session.get('page_number')===1){
			return 'disabled';
		}
	},

	savedPlantProblemData:function(){
		var selector = {};
		d_cnameFilter=Session.get('d_cnameFilter');
		d_fnameFilter= Session.get('d_fnameFilter');
		snameFilter=Session.get('snameFilter');
		tstageFilter= Session.get('tstageFilter');
		d_paffectedFilter=Session.get('d_paffectedFilter');
		rpestFilter= Session.get('rpestFilter');

		if (d_cnameFilter !== undefined) {
			selector.cname = d_cnameFilter;
		}

		if (d_fnameFilter !== undefined) {
			selector.fname = d_fnameFilter;
		}

		if (d_paffectedFilter !== undefined) {
			selector.paffected = d_paffectedFilter;
		}
		if (rpestFilter !== undefined) {
			selector.rpest = rpestFilter;
		}
		if (snameFilter !== undefined) {
			selector.sname = snameFilter;
		}
		if (tstageFilter !== undefined) {
			selector.tstage = tstageFilter;
		}
    	// console.log(selector);
    	return selector;
    }

});

// Template.SaraiDiseasesLib.onCreated(function() {
// 	Meteor.subscribe('plantProblems');
// 	//Meteor.subscribe('cms');
// });

Template.PlantProblemResult.helpers({
	imageName: function(str){
		return str.replace(/\s/g, '');
	}
})
